###################
## SETUP PACE.JS ##
###################

[globalVar = LIT:1 = {$plugin.tx_hive_thm_pace.settings.lib.pace.bUsePace}] && [globalVar = LIT:1 > {$plugin.tx_hive_cfg_typoscript.settings.gulp}]
    page {
        includeCSS {
            pace_css = {$plugin.tx_hive_thm_pace.settings.production.includePath.public}Assets/Css/pace.css
            #pace_css = {$plugin.tx_hive_thm_pace.settings.production.includePath.private}Assets/Less/includeCSS/pre/pace.less
            pace_css.forceOnTop = 1
            pace_css.disableCompression = 1
            pace_css.excludeFromConcatenation = 1
        }
        includeJSLibs {
            pace_js = {$plugin.tx_hive_thm_pace.settings.production.includePath.public}Assets/Js/pace.min.js
            pace_js.excludeFromConcatenation = 1
            pace_js.disableCompression = 1
            pace_js.forceOnTop = 1
        }
    }
[else]
    page {
        headerData.3330011 = TEXT
        headerData.3330011.value = <script type="text/javascript">
        headerData.3330012 = TEXT
        headerData.3330012.dataWrap = var hive_thm_pace_bUsePace = {$plugin.tx_hive_thm_pace.settings.lib.pace.bUsePace};
        headerData.3330013 = TEXT
        headerData.3330013.value = </script>
    }
[global]